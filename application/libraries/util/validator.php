<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH.'libraries/util/CI_Object.php';

 class validator extends CI_Object {

    public function validateUser() {
        $this->form_validation->set_rules('nome', 'nome da pessoa', 'required | min_length[2] | max_lenght [19]');
        $this->form_validation->set_rules('snome', 'sobrenome da pessoa', 'required | min_length[2] | max_lenght[256]');
        $this->form_validation->set_rules('email', 'endereço eletrônico', 'required | valid_email');
        $this->form_validation->set_rules('senha', 'senha', 'required | min_length[8] | max_lenght[32]');
        $this->form_validation->set_rules('conf_senha', 'confirmação da senha', 'required | matches[senha]');
        $this->form_validation->set_rules('telefone', 'telefone','required | ,  min_length[8] | max_lenght[14]');
    }
 }